from bs4 import BeautifulSoup
import requests


#OF7

#get html file from url
url="https://cfd.direct/openfoam/user-guide/v7-standard-solvers/"

html_content = requests.get(url).text

soup = BeautifulSoup(html_content, "lxml")

#get the data from the table
cikk = soup.find("div", attrs={"class": "entry-content"})

tabledata = cikk.find_all("dl")

#define the list of solvers
solvers7 = []
#iterate trough html table, saving every solver to solvers7
for dl in tabledata:
    for dt in dl.find_all("dt"):
        solvers7.append(dt.get_text().replace(" ", ""))


#OF19

#else redirects me to forbidden page
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

url2="https://www.openfoam.com/documentation/user-guide/standard-solvers.php"

html_content2 = requests.get(url2,headers=headers).content

soup = BeautifulSoup(html_content2, "lxml")
#here i just saved html to a string, then saved every word what contained 'Foam' to a list
cikk = soup.find("table", attrs={"class": "longtable"})

table_text=cikk.get_text().replace(" ", "")

splits = table_text.split('\n')

solvers19 = [x for x in splits if 'Foam' in x]

#Compate the two list, saves the difference
of7_only = list(set(solvers7) - set(solvers19))
of19_only = list(set(solvers19) - set(solvers7))
print(of7_only)
print('\n')
print(of19_only)